from concurrent import futures

import grpc
import yaml
import time
import messenger_pb2
import messenger_pb2_grpc

PORT = 'port'
GROUPS = 'groups'
_ONE_DAY_IN_SECONDS = 60 * 60 * 24
MAX_CALL_PER_30_SECONDS_PER_USER='max_call_per_30_seconds_per_user'
MAX_NUM_MESSAGES_PER_USER='max_num_messages_per_user'


class Messanger(messenger_pb2_grpc.MessengerServicer):

    def __init__(self):
        self.lruCache = []    #LRU Cache
        self.dictionary = {}

    def rate(func):
        def wrapper(self, request, context):
            user = request.sender
            if user in self.dictionary:
                times = []
                times = self.dictionary.get ( user )
                if len ( times ) == int ( rateLimit ):
                    currenttime=time.time()
                    if (currenttime -times[0])>30:
                        while len ( self.lruCache ) >= int ( capacity ):  # LRU Cache implementation,capacity = MAX_NUM_MESSAGES_PER_USER
                            del self.lruCache[0]
                        time.sleep ( 0.05 )
                        self.lruCache.append ( request )
                        del times[0]
                        times.append(time.time())
                        self.dictionary[user] = times
                    else:
                        context.set_code(grpc.StatusCode.RESOURCE_EXHAUSTED)
                        context.set_details("User messages ratelimit exceeded,try after 30 sec")
                else:
                    while len ( self.lruCache ) >= int ( capacity ):  # LRU Cache implementation,capacity = MAX_NUM_MESSAGES_PER_USER
                        del self.lruCache[0]
                    time.sleep ( 0.05 )
                    self.lruCache.append ( request )
                    times.append(time.time())
                self.dictionary[user] = times
            else:
                while len ( self.lruCache ) >= int ( capacity ):  # LRU Cache implementation,capacity = MAX_NUM_MESSAGES_PER_USER
                    del self.lruCache[0]
                time.sleep ( 0.05 )
                self.lruCache.append ( request )
                times=[]
                times.append(time.time())
                self.dictionary[user] = times
            result = func ( self, request, context )
            return result

        return wrapper

    @rate
    def SendMessage(self, request, context):
        try:
            return messenger_pb2.Empty()
        except yaml.YAMLError as exc:
            print(exc)

    def ReceiveMessage(self, request, context):
        lastIndex=0
        while True:
            while len(self.lruCache) < lastIndex:
                lastIndex-=1
            while len(self.lruCache) > lastIndex:
                msg = self.lruCache[lastIndex]
                lastIndex += 1
                yield msg

    def FindGroup(self, request, context):
        with open('config.yaml', 'r') as stream:
            try:
                groups = yaml.load(stream).get(GROUPS)
                for key, value in groups.items():
                    if request.name in value:
                        global groupName
                        groupName = key
                        global groupMembers
                        groupMembers = value
                        return messenger_pb2.FindGroupReply ( groupName=groupName, groupMembers=groupMembers )
                if request.name not in value:
                     context.set_code(grpc.StatusCode.NOT_FOUND)
                     context.set_details("user not found in group")
                     return messenger_pb2.FindGroupReply ( groupName=None, groupMembers=None )

            except yaml.YAMLError as exc:
                print(exc)

def getCacheCapacity():
    with open ( 'config.yaml', 'r' ) as stream:
        try:
            cacheCapacity = yaml.load ( stream ).get ( MAX_NUM_MESSAGES_PER_USER )
            return str ( cacheCapacity )
        except yaml.YAMLError as exc:
            print ( exc )

def getRateLimit():
    with open ( 'config.yaml', 'r' ) as stream:
        try:
            cacheCapacity = yaml.load ( stream ).get ( MAX_CALL_PER_30_SECONDS_PER_USER )
            return str ( cacheCapacity )
        except yaml.YAMLError as exc:
            print ( exc )


def getPort():
    with open('config.yaml', 'r') as stream:
        try:
            port = yaml.load(stream).get(PORT)
            return str(port)
        except yaml.YAMLError as exc:
            print(exc)

def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    messenger_pb2_grpc.add_MessengerServicer_to_server(Messanger(), server)
    port = getPort()
    server.add_insecure_port('[::]:' + port)
    server.start()
    print('Spartan server started on port', port, end='.\n')
    global capacity
    global rateLimit
    capacity = getCacheCapacity ()
    rateLimit = getRateLimit ()
    try:
        while True:
            time.sleep(_ONE_DAY_IN_SECONDS)
    except KeyboardInterrupt:
        print('interrupted')
        server.stop(0)

if __name__ == '__main__':
    serve()