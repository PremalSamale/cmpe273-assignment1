import threading
import sys
import messenger_pb2
import messenger_pb2_grpc
import grpc
import yaml
from Crypto.Cipher import AES
import base64

PORT = 'port'
LOCALHOST = 'localhost:'
USER_NAME_AS_CMD_ARG = 'Please enter the user\'s name as Command Line Argument'
CONNECTED_TO_SERVER = '[Spartan] Connected to Spartan Server at port '
KEY='abcdefghijklmnopqrstuvwxyz123456'

BS = 16
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS).encode()
unpad = lambda s: s[:-ord(s[len(s)-1:])]

def iv():

    return chr(0) * 16


def encrypt(message):

    message = message.encode()
    raw = pad(message)
    key = KEY
    cipher = AES.new(key, AES.MODE_CBC, iv())
    enc = cipher.encrypt(raw)
    return base64.b64encode(enc).decode('utf-8')

def decrypt(enc):
    enc = base64.b64decode(enc)
    key = KEY
    cipher = AES.new(key, AES.MODE_CBC, iv())
    dec = cipher.decrypt(enc)
    return unpad(dec).decode('utf-8')

def getPort():
    with open('config.yaml', 'r') as stream:
        try:
            port = yaml.load(stream).get(PORT)
            return str(port)
        except yaml.YAMLError as exc:
            print(exc)

def sendMessage():
    while True:
        message = input()
        encryptedMessage=encrypt(message)
        try:
            stub.SendMessage(messenger_pb2.Message(sender=clientName, message=encryptedMessage))
        except grpc.RpcError as e:
            print('User exceeded the message rate limit.Try again after 30 seconds')


def receiveMessage():
    for msg in stub.ReceiveMessage(messenger_pb2.Empty()):
        try:
            groupNameClient = stub.FindGroup(messenger_pb2.FindGroupRequest(name=clientName))
        except grpc.RpcError as e:
            pass
        else:
            groupNameSender = stub.FindGroup(messenger_pb2.FindGroupRequest(name=msg.sender))
            decryptedMessage=decrypt(msg.message)
            if(clientName!=msg.sender and groupNameClient == groupNameSender ):
                print("[{}] {}".format(msg.sender,decryptedMessage ))



if __name__ == '__main__':
    cmdArgs = sys.argv
    if(len(cmdArgs) < 2):
        print(USER_NAME_AS_CMD_ARG)
        exit(1)
    clientName = cmdArgs[1]
    port = getPort()
    with grpc.insecure_channel(LOCALHOST+port) as channel:
        global stub
        stub = messenger_pb2_grpc.MessengerStub(channel)
        print(CONNECTED_TO_SERVER, port, end=".\n")
        try:
            response = stub.FindGroup(messenger_pb2.FindGroupRequest(name=clientName))
        except grpc.RpcError as e:
            pass
            print("invalid user")
            exit(1)
        else:
            print ( '[Spartan] Group list:', response.groupMembers )
            print ( '[Spartan] You are now ready to chat with', response.groupName )

        threadLock=threading.Lock()
        threads=[]
        t1 = threading.Thread(target=sendMessage)
        t2 = threading.Thread(target=receiveMessage)
        t1.start()
        t2.start()
        threads.append(t1)
        threads.append(t2)
        for t in threads:
            t.join()
